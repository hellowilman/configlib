// by Wilman

#ifndef CONFIG_H
#define CONFIG_H
#include <map>
#include <string>
#include "configlib.h"

class Config:public iConfig
{
    public:
        Config();
        ~Config();

        int load(const char* pConfigName);
        int save(const char* pConfigName);

        int getInt(std::string key, int def=0);
        double getDouble(std::string key, int def=0.0);
        std::string getStr(std::string key, std::string def="");
		std::string get(std::string key, std::string def="");

        void setInt(std::string key, int val=0);
        void setDouble(std::string key, double val=0.0);
        void setStr(std::string key, std::string val="");

    private:
        std::map<std::string, std::string>   data;
        std::map<std::string, std::string>   comment_data;
};

#endif // CONFIG_H
