// by Wilman @ ASTRI 2014 
#include "config.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
using namespace std; 


Config::Config()
{
}


Config::~Config()
{
}

string trim(string str)
{
	int k = 0, N = str.length();
	int st = 0, ed = N-1;

	while(k< N && str[k++] == ' ');
	st = k-1; 
	k = N -1;
	while(k>0 && str[k--] == ' ');
	ed = k+1;
	string out= str.substr(st,ed-st+1);
	return out;
}

int Config::load(const char* pConfigName)
{
	ifstream fio(pConfigName);
	if(!fio)
    {
		return -1;
	}
	string line;
    string comment = "";
	while(getline(fio,line))
    {
		// parser
		if(line.length() > 0 && line[0] != '#')
		{
			int inx = line.find_first_of('=');
			string key = line.substr(0,inx);
			key = trim(key);
			
			string val = line.substr(inx+1);
			val = trim(val);
			data[key.c_str()] = val;
            if(comment.length() > 0){
                comment_data[key.c_str()] = comment;
            }
            comment = "";
			//cout<<"line:"<<line << " k-v:"<<key<<"-" <<val<<endl;
		}
        else
        { // line.length() == 0 or line[0] == '#'
//            if(line.length() ==0){
//                comment = "";
//            }else{
//                comment += line + "\n";
//            }
            comment += line + "\n";
			//cout <<"comment:" << line <<endl;
		}
		
	}
	
	return 0;
}

int Config::save(const char* pConfigName)
{
    ofstream out(pConfigName);
    if(!out)
        return -1;

	map<string,string>::iterator it;
    for(it=data.begin(); it!=data.end(); it++){
        string comment = comment_data[it->first];
        if(comment.length() > 0){
            out << comment;
        }else{
            out << "\n";
        }
        out << it->first << "=" << it->second << "\n";
    }

    out.close();
    return 0;
}

std::string Config::getStr(std::string key, std::string def)
{
	map<string,string>::iterator it;
	it = data.find(key);
	if(it != data.end())
    {
		return it->second;
	}
    else
    {
		return def;
	}
}

int Config::getInt(std::string key,int def)
{
	string val = getStr(key);
	int out =  atoi(val.c_str());
		if(val.length()>0)
        {
			return out;
		}
        else
        {
			return def;
		}

	return out;
}

double Config::getDouble(std::string key, int def)
{
	string val = getStr(key);
    char *end = NULL;
    double d = strtod(val.c_str(), &end);
    if(*end)
        return def;
    return d;
}

void Config::setInt(std::string key, int val)
{
	long long lVal = val;
    if(!key.empty())
        data[key] = std::to_string(lVal);
}

std::string Config::get(std::string key, std::string def){
	return getStr(key,def);
}

void Config::setDouble(std::string key, double val)
{
    if(!key.empty())
    {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(15) << val;
        data[key] = ss.str();
    }
}

void Config::setStr(std::string key, std::string val)
{
    if(!key.empty())
        data[key] = val;
}
