//by Wilman 

#ifndef CONFIGLIB_H
#define CONFIGLIB_H

#define AS_API 

#include <map>
#include <string>

class iConfig
{
    public:
		AS_API virtual ~iConfig(){};
        AS_API int virtual load(const char* pConfigName)=0;
        AS_API int virtual save(const char* pConfigName)=0;

		/**
		* 
		* @brief  get data from the config file  
		* @return 
		*/
        AS_API int virtual getInt(std::string key, int def=0)=0;
        AS_API double virtual getDouble(std::string key, int def=0.0)=0;
        AS_API std::string virtual getStr(std::string key, std::string def="")=0;
		AS_API std::string virtual get(std::string key, std::string def="")=0;

        AS_API void virtual setInt(std::string key, int val=0)=0;
        AS_API void virtual setDouble(std::string key, double val=0.0)=0;
        AS_API void virtual setStr(std::string key, std::string val="")=0;
};

AS_API iConfig* createConfig();
#endif // IMAGECAPTURER_H
