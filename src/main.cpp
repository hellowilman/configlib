//
//  main.cpp
//  
//
//  Created by wilman on 2/6/15.
//
//

#include "main.h"
#include "config.h"

int main(){
    Config cfg;
    cfg.load("cfg.ini");
    int h= cfg.getInt("hello");
    printf("hello is %d\n",h);
    cfg.setStr("val_add_by_main","this is added by main program");
    cfg.save("cfg2.ini");
    return 0;
}